import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static org.eclipse.microprofile.health.HealthCheckResponse.State.DOWN;
import static org.eclipse.microprofile.health.HealthCheckResponse.State.UP;
import static org.eclipse.microprofile.health.HealthCheckResponse.named;

@Stateless
@Path("/health")
public class ServiceHealthCheck implements HealthCheck {

	@GET
	public HealthCheckResponse call() {
		final HealthCheckResponseBuilder builder = named("GoogleHealth");
		final String url = "http://www.google.com";
		try {
			HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setRequestMethod("HEAD");
			int responseCode = connection.getResponseCode();

			if (responseCode >= 200 || responseCode <300) {
				builder.withData(url, UP.toString());
				builder.up();
			} else {
				builder.withData(url, DOWN.toString());
				builder.down();
			}
		} catch (IOException ioExc) {
			ioExc.printStackTrace();
			builder.withData(url, DOWN.toString());
			builder.down();
		}
		return builder.build();
	}
}
